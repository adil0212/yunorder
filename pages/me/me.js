Page({
    data: {
        userInfo: ''
    },
    onLoad() {
        let user = wx.getStorageSync('user')
        this.setData({
            userInfo: user
        })
    },
    login() {
        wx.getUserProfile({
            desc: '必须授权才可以继续使用',
            success: res => {
                let user = res.userInfo
                wx.setStorageSync('user', user)
                this.setData({
                    userInfo: user
                })
            },
            fail: res => {
                console.log('授权失败', res)
            }
        })
    },
    // 退出登录
    loginOut() {
        this.setData({
            userInfo: ''
        })
        wx.setStorageSync('user', null)
    },
    // 去我的订单页
    goMyorder() {
        wx.navigateTo({
            url: '/pages/myOrder/myOrder',
        })
    },
    // 去我的评价页
    goMycomment() {
        wx.navigateTo({
          url: '/pages/mycomment/mycomment',
        })
    },
    // 去管理员页
    goAdmin(){
        wx.navigateTo({
          url: '/pages/admin/admin',
        })
    },
    gopaihao() {
        wx.navigateTo({
          url: '/pages/paihao/paihao',
        })
    }
})