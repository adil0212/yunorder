const app = getApp()
const db = wx.cloud.database()
Page({
    onShow() {
        this.getNum()
    },
    getNum() {
        db.collection('paihao').doc(app.getCurrentYMD()).get()
            .then(res => {
                console.log('今日排号数据', res)
                this.setData({
                    paihao: res.data
                })
            })
            .catch(res => {
                console.log('今日没有排号', res)
            })
    },
    jiaohao(e) {
        let type = e.currentTarget.dataset.type
        console.log(type)
        wx.cloud.callFunction({
            name: 'paihao',
            data: {
                action: 'admin',
                id: app.getCurrentYMD(),
                type: type
            }
        }).then(res => {
            this.getNum()
        })
    }
})