// pages/home/home.js
const db = wx.cloud.database()
let searchKey = ''
Page({
    data: {
        list: [],
        foodList: []
    },
    onLoad: function (options) {
        this.getTopList()
        this.getHotList()
    },
    // 获取用户输入的内容
    getSearch(e) {
        searchKey = e.detail.value
    },
    // 触发搜索事件
    goSearch() {
        console.log('触发了搜索事件', searchKey)
        if (searchKey && searchKey.length > 0) {
            // 搜索的触发写在这里
            wx.navigateTo({
                url: '/pages/food/food?searchKey=' + searchKey,
            })
        } else {
            wx.showToast({
                icon: 'none',
                title: '搜索词为空',
            })
        }
    },
    // 点击事件： 去菜品页
    click1() {
        wx.navigateTo({
          url: '/pages/pay/pay',
        })
    },
    click2() {
        wx.navigateTo({
            url: '/pages/food/food',
        })
    },
    click3() {
        wx.navigateTo({
            url: '/pages/paihao/paihao',
        })
    },
    click4() {
        wx.navigateTo({
            url: '/pages/address/address',
        })
    },
    // 获取首页轮播图
    getTopList() {
        db.collection('lunbotu').get()
            .then(res => {
                console.log('获取轮播图成功', res)
                this.setData({
                    list: res.data
                })
            })
            .catch(res => {
                console.log('获取轮播图失败', res)
                this.setData({
                    list: [{
                        picUrl: '../../image/2.jpg'
                    }, {
                        picUrl: '../../image/3.jpg'
                    }, {
                        picUrl: '../../image/4.png'
                    }]
                })
            })
    },
    // 获取热门菜品列表
    getHotList() {
        // 小程序端直接调取数据库
        // db.collection('food')
        //     .where({
        //         status: "上架"
        //     })
        //     .orderBy("sell", "desc")
        //     .limit(10)
        //     .get()
        //     .then(res => {
        //         console.log('菜品列表', res)
        //     })
        // 通过云函数调用数据
        wx.cloud.callFunction({
                name: 'getFoodList2'
            })
            .then(res => {
                console.log('菜品列表', res)
                this.setData({
                    foodList: res.result.data
                })
            })
    }
})