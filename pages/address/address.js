Page({
    data: {
        address: '沙雅县古勒巴格镇政府',
        phone: '18599274413',
        weixin: '18599274413',
        // 标记点
        markers: [{
            id: 0,
            name: '沙雅县古勒巴格镇政府',
            address: '新疆维吾尔自治区阿克苏地区沙雅县古勒扎尔南路',
            latitude: '41.201171',
            longitude: '82.820522',
            width: 50,
            height: 50
        }]
    },
    clickMap(e) {
        console.log(e.currentTarget.dataset.marker)
        let marker = e.currentTarget.dataset.marker
        wx.getLocation({
            type: 'wgs84',
            success(res) {
                wx.openLocation({
                    latitude: marker.latitude,
                    longitude: marker.longitude,
                    name: marker.name,
                    address: marker.address,
                    scale: 18
                })
            },
            fail(res) {
                wx.showModal({
                    title: '需要授权',
                    content: '需要授权位置信息，才可以实现导航，点击去设置就可以开启位置权限',
                    confirmText: '去设置',
                    success(res) {
                        if (res.confirm) {
                            wx.openSetting()
                        }
                    }
                })
            }
        })
    },
    // 拨打电话
    callPhone(e) {
        wx.makePhoneCall({
          phoneNumber: 'e.currentTarget.dataset.phone',
        })
    },
    copyWechat(e) {
        wx.setClipboardData({
          data: 'e.currentTarget.dataset.weixin',
        })
    }
})