// pages/paihao/paihao.js
const db = wx.cloud.database()
const _ = db.command
const app = getApp()
Page({
    data: {
        renshu: 0,
        renshulist: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    },
    onShow() {
        console.log('日期', app.getCurrentYMD())
        this.getNum()
    },
    // 选择就餐人数
    xuanzhong(e) {
        this.setData({
            renshu: e.currentTarget.dataset.item
        })
    },
    // 点击排号小桌
    paihaoXiao() {
        this.paihao(1)
    },
    // 点击排号大桌
    paihaoDa() {
        this.paihao(2)
    },
    // 排号的方法
    paihao(type) {
        let renshu = this.data.renshu
        if (renshu <= 0) {
            wx.showToast({
                icon: 'error',
                title: '请选择就餐人数',
            })
            return
        }
        if (type == 1 && renshu > 4) { //大桌
            wx.showToast({
                icon: 'error',
                title: '请选择大桌',
            })
            return
        }
        if (type == 2 && renshu < 5) { //小桌
            wx.showToast({
                icon: 'error',
                title: '请选择小桌',
            })
            return
        }
        let date = app.getCurrentYMD()
        db.collection('paihao')
            .doc(date).get()
            .then(res => {
                if (res.data && res.data._id) {
                    wx.cloud.callFunction({
                        name: 'paihao',
                        data : {
                            action: 'user',
                            id: app.getCurrentYMD(),
                            type: type
                        }
                    }).then(res => {
                        this.getNum()
                    })
                }
            })
            .catch(res => {
                console.log('查询的数据不存在')
                let dataObj = {
                    _id: date,
                    date: app.getCurrentYMDStr(),
                    xiaozhuoNum: 0,
                    dazhuonum: 0,
                }
                if (type == 1) {
                    dataObj.xiaozhuo = [app.globalData.openid]
                } else {
                    dataObj.dazhuo = [app.globalData.openid]
                }
                db.collection('paihao')
                    .add({
                        data: dataObj
                    }).then(res => {
                        console.log('排号等位添加的结果', res)
                        this.getNum()
                    })
            })
    },

    getNum() {
        db.collection('paihao').doc(app.getCurrentYMD()).get()
            .then(res => {
                console.log('今日排号数据', res)
                let item = res.data
                if (item && item._id) {
                    let xiaozhuo = item.xiaozhuo
                    let dazhuo = item.dazhuo
                    this.setData({
                        xiaozhuoNum: xiaozhuo ? xiaozhuo.lastIndexOf(app.globalData.openid) + 1 : 0,
                        dazhuoNum: dazhuo ? dazhuo.lastIndexOf(app.globalData.openid) + 1 : 0,
                        xiaozhuoDangqian: item.xiaozhuonum,
                        dazhuoDangqian: item.dazhuonum
                    })
                }
            })
            .catch(res => {
                console.log('今日没有排号', res)
            })
    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})