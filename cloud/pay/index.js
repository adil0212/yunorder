// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const tenpay = require('tenpay')

const config = {
    appid: 'wxfa34bc66e840233e', //app id
    mchid: '18599274413',  // 微信商户号
    partnerKey: 'adiljan0601', // 微信支付安全密钥
    // pfx: require('fs').readFileSync(''), //证书文件路径
    notify_url: 'https://www.jianshu.com', //支付回调网址
    spbill_create_ip: '127.0.0.1' //IP地址
}

// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext()

    return {
        openid: wxContext.OPENID,
    }

    // 初始化
    const api = tenpay.init(config)

    // 获取支付参数
    let result = await api.getPayParams({
        out_trade_no: '123456789', //商户内部订单号
        body: '商品简单描述', //商品简单描述
        total_fee: 1, //订单金额（分）
        openid: 'wxContext.OPENID', //付款用户的openid
    })
    return result
}