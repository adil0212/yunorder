// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV
})

// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext()
    const db = cloud.database()
    const _ = db.command

    if (event.action == 'user') { //用户调用云函数
        let dataObj = {}
        if (event.type == 1) {
            dataObj = {
                xiaozhuo: _.push(wxContext.OPENID)
            }
        } else {
            dataObj = {
                dazhuo: _.push(wxContext.OPENID)
            }
        }
        return await db.collection('paihao').doc(event.id)
            .update({
                data: dataObj
            })
    } else if (event.action == 'admin') { // 管理员调用云函数
        if (event.type == 1) {
            return await db.collection('paihao')
                .doc(event.id)
                .update({
                    data: {
                        xiaozhuonum: _.inc(1)
                    }
                })
        } else if (event.type == 2) {
            return await db.collection('paihao')
                .doc(event.id)
                .update({
                    data: {
                        dazhuonum: _.inc(1)
                    }
                })
        }
    }
}