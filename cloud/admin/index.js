// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV
})

// 云函数入口函数
const db = cloud.database()
exports.main = async (event, context) => {
    if (event.type == 'get') {
        return await db.collection('order')
            .where({
                status: event.status
            }).get()
    } else if (event.type == 'update') {
        return await db.collection('order').doc(event.id)
            .update({
                data: {
                    status: event.status
                }
            })
    }
}