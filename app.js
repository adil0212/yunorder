// app.js
App({
  globalData: {
    zhuohao: null,
    openid: null
  },
  onLaunch() {
    wx.cloud.init({
      env: 'cloud1-8gyoi4z8098fbdbf'
    })
    this.getopenid()
  },
  // 获取用户的openid
  getopenid() {
    wx.cloud.callFunction({
      name: 'getOpenid'
    }).then(res => {
      console.log(res.result.openid)
      this.globalData.openid = res.result.openid
    })
  },
  // 获取当前时间
  getCurrentTime() {
    let d = new Date();
    let month = d.getMonth() + 1;
    let date = d.getDate();
    let hours = d.getHours();
    let minutes = d.getMinutes();

    let curDateTime = d.getFullYear() + '年';
    if (month > 9)
      curDateTime += month + '月';
    else
      curDateTime += month + '月';
    if (date > 9)
      curDateTime = curDateTime + date + '日';
    else
      curDateTime = curDateTime + date + '日';
    if (hours > 9)
      curDateTime = curDateTime + hours + '时';
    else
      curDateTime = curDateTime + hours + '时';
    if (minutes > 9)
      curDateTime = curDateTime + minutes + '分';
    else
      curDateTime = curDateTime + minutes + '分';
    return curDateTime;
  },
  // 获取当前的年月日
  getCurrentYMD() {
    let d = new Date()
    let year = d.getFullYear()
    let month = d.getMonth() + 1
    let date = d.getDate()
    let time = "" + year + month + date
    return time
  },
  // 获取当前的年月日
  getCurrentYMDStr() {
    let d = new Date()
    let year = d.getFullYear()
    let month = d.getMonth() + 1
    let date = d.getDate()
    let time = year + "年" + month + "月" + date + "日"
    return time
  },
})